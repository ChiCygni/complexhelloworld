#include <iostream>
#include <functional>
#include <memory>

int main()
{
    return [cout = std::ref(std::cout)]<typename T> (T* i) mutable noexcept -> T
    {
        return static_cast<T>(static_cast<bool>(std::endl(operator<<(cout.get(), "Hello world!")))) ^ ++(*i);
    }
    (std::make_unique<int>(0).get());
}